# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

### Fixed

### Changed

### Removed


## [0.1.0] 2025-02-07

### Added

- Race mode setting to referee app

### Changed

- Versioning pattern. This app is now out of beta.


## [0.0.12] 2025-01-22

### Changed

- Game max break increased from 120 to 180
- Move referee setting from refereeStore.general.raceMode to refereeStore.raceMode

### Removed

- Unused segment configs and elements


## [0.0.11] 2024-12-16

### Fixed

- Reset settings button not working

### Removed

- Referee mode setting


## [0.0.10] 2024-12-16

### Added

- Version string in Main Menu
- Settings for possession timer
  - Can now setup the possession timer between 10 and 20 seconds
  - Can now setup whehther the 5 second notification should be applied

### Changed

- Merged Race Referee app into Referee app. Race Referee still WIP.


## [0.0.9] 2024-09-18

### Fixed

- Double touch/mouse event on mobile

### Changed

- Minor layout adjustmens for referee apps


## [0.0.8] 2024-09-18

### Added

- Race Referee Application


## [0.0.7] 2024-02-07

### Fixed

- Button text sometimes being selected when long pressing the button


## [0.0.6] 2023-06-08

### Added

- Analyzer app description on home screen

### Fixed

- Timer millis being re-calculated with wrong stop time even though the timer stopped already
- Notification on reset vilolation

### Changed

- Slightly adjusted header navigation of individual apps


## [0.0.5] 2023-06-06

### Added

- Referee settings
- LocalStorageStore object to create svelte writable store with localStorage support
- Analyzer App

### Fixed

- Timers sending stop events on component destroy
- Timers not clearing interval on restart (stop -> start)
- Make settings persistent
- Minor UI fixes

### Changed

- Extract NotificationService to handle sound and vibration notifications

### Removed


## [0.0.4] 2023-05-31

### Fixed

- Respect user notification settings in Trainer app
- Fix browser refresh resulting in 404

### Changed

- Update service worker with sveltekit documented service worker


## [0.0.3] 2023-05-31

### Added

- Configuration posibillity for Timer component and object
- Trainer app and settings

### Fixed

- General layout fixes

### Changed

- Routing to avoid building issues

### Removed

- Currently not implemented apps


## [0.0.2] 2023-05-30

### Added

- Avoid screen turning off in Referee app
- Dark/Light mode switch
- Add background color for reset violation

### Fixed

- Fingerprint sensor triggering key events on timer buttons
- `navigator.vibrate` causing Errors on WebKit browsers
- Fix disabled buttons being shown on top of navigation
- Several issues with the `Header` component
- Fix reset buttons flashing on posession reset

### Changed

- Minor header style changes


## [0.0.1] - 2023-05-26

### Added

- Referee app
