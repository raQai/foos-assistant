/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Trainer Settings',
        navigation: {
            back: true
        }
    };
}
