/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Trainer',
        navigation: {
            settings: '/apps/trainer/settings'
            // stats: '/apps/trainer/stats'
        }
    };
}
