/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Analyzer Stats',
        navigation: {
            back: true
        }
    };
}
