/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Analyzer',
        navigation: {
            stats: '/apps/analyzer/stats',
            settings: '/apps/analyzer/settings'
        }
    };
}
