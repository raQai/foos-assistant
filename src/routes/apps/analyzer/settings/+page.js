/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Analyzer Settings',
        navigation: {
            back: true
        }
    };
}
