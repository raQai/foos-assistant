/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Referee Settings',
        navigation: {
            back: true
        }
    };
}
