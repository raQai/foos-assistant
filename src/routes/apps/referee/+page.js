/** @type {import('./$types').PageLoad} */
export async function load() {
    return {
        title: 'Referee',
        navigation: {
            settings: '/apps/referee/settings'
        }
    };
}
