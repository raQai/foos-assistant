export const Reset = {
    RESET: 0,
    WARNING: 1,
    VIOLATION: 2
};

export const ResetLabel = {
    [Reset.RESET]: 'reset',
    [Reset.WARNING]: 'warning',
    [Reset.VIOLATION]: 'violation'
};
