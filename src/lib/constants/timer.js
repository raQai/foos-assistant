export const Event = {
    STARTED: 'timer.started',
    STOPPED: 'timer.stopped',
    RESET: 'timer.reset'
};

export const State = {
    RUNNING: 'timer.running',
    STOPPED: 'timer.stopped'
};
