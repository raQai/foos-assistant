import Logger from '$lib/objects/Logger.js';
import SoundPlayer from '$lib/objects/SoundPlayer.js';

let service = null;

const logger = Logger('NotificationService');

class NotificationService {
    constructor(AudioContext, navigator) {
        if (AudioContext) {
            this._soundPlayer = new SoundPlayer(new AudioContext());
        }
        if ('vibrate' in navigator) {
            this._navigator = navigator;
        }
        Object.freeze(this);
        logger.log(this);
    }

    playSound(volume) {
        if (this._soundPlayer && volume > 0) {
            this._soundPlayer
                .play(600, 0.01, 'sine')
                .setFrequency(1800, 0.08)
                .setVolume(volume, 0.02)
                .setVolume(0.01, 0.19)
                .stop(0.2);
        }
    }

    vibrate(pattern) {
        this._navigator?.vibrate(pattern);
    }

    /**
     *
     * @param {{
     *      sound: ({enabled: boolean, volume: number}|undefined)
     *      vibrate: ({enabled: boolean, pattern: number[]}|undefined)
     * }} params
     */
    notify({ sound, vibrate }) {
        if (sound?.enabled) {
            this.playSound(sound.volume);
        }
        if (vibrate?.enabled) {
            this.vibrate(vibrate.pattern);
        }
    }

    release() {
        this._soundPlayer.stop();
        service = null;
    }
}

function notify(...args) {
    return service?.notify(...args);
}

export default {
    getInstance: ({ AudioContext, navigator }) => {
        if (service) {
            return service;
        }

        service = new NotificationService(AudioContext, navigator);
        logger.log('service instantiated', service);
    },
    notify
};
