import { localStorageWritable } from '$lib/objects/LocalStorageStore.js';

export const settings = localStorageWritable('fass.trainer', {
    positions: 5,
    setup: {
        seconds: 3,
        auto: true,
        notification: {
            seconds: 0,
            sound: { enabled: true, volume: 0.3 },
            vibrate: { enabled: true, pattern: [50, 50, 50] }
        }
    },
    trigger: {
        secondsMin: 3,
        secondsMax: 15,
        notification: {
            seconds: 0,
            sound: { enabled: true, volume: 0.3 },
            vibrate: { enabled: true, pattern: [150] }
        }
    }
});
