import { localStorageWritable } from '$lib/objects/LocalStorageStore.js';

export const stats = localStorageWritable('fass.analyzer.stats', []);
