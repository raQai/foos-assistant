import { localStorageWritable } from '$lib/objects/LocalStorageStore.js';
import { Theme } from '$lib/constants/theme.js';

export const theme = localStorageWritable('fass.theme', Theme.LIGHT);
