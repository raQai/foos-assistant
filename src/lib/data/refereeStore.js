import { localStorageWritable } from '$lib/objects/LocalStorageStore.js';
import { Reset } from '$lib/constants/reset.js';

export const settings = localStorageWritable('fass.referee', {
    notification: {
        vibrate: {
            enabled: true
        }
    },
    raceMode: false,
    game: {
        firstServe: 1,
        firstTable: 1,
        break: 90
    },
    segment: {
        count: 7,
        goals: 6,
        break: 180
    },
    timeout: {
        count: 2,
        seconds: 30
    },
    possession: {
        seconds: 15,
        fiveSecondsNotification: true
    }
});

export const defaults = {
    team: {
        timeout: 0,
        goals: 0,
        reset: Reset.RESET
    },
    timeout: {
        maxSeconds: 60,
        minSeconds: 30,
        maxCount: 3,
        minCount: 1
    },
    game: {
        maxBreak: 180,
        minBreak: 60
    },
    possession: {
        maxSeconds: 20,
        minSeconds: 10
    },
    timers: {
        possession: {
            notifications: [
                {
                    seconds: 5,
                    vibrate: { enabled: true, pattern: [200, 50, 100] },
                    color: 'rgb(255, 179, 0)'
                },
                {
                    seconds: 0,
                    vibrate: { enabled: true, pattern: [1000] },
                    color: 'rgb(229, 57, 53)'
                }
            ]
        },
        game: {
            notifications: [
                {
                    seconds: 30,
                    vibrate: { enabled: true, pattern: [200, 50, 100] }
                },
                {
                    seconds: 0,
                    vibrate: { enabled: true, pattern: [1000] }
                }
            ]
        },
        timeout: {
            notifications: [
                {
                    seconds: 10,
                    vibrate: { enabled: true, pattern: [200, 50, 100] }
                },
                {
                    seconds: 0,
                    vibrate: { enabled: true, pattern: [1000] }
                }
            ]
        }
    }
};
