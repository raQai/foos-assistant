import { State as TimerState } from '$lib/constants/timer.js';
import Logger from '$lib/objects/Logger.js';

/**
 * @typedef {Object} Timer
 * @property {string} id
 * @property {function} start
 * @property {function} stop
 * @property {function} reset
 */

/**
 * @returns {Timer}
 */
export default ({
    id,
    seconds = 0,
    onChange = () => {},
    beforeStart = () => {},
    afterStop = () => {},
    onReset = () => {}
}) => {
    const logger = Logger(`Timer#${id}`);
    let state = TimerState.STOPPED;
    let _seconds = seconds;
    let time = _seconds * 1000;
    let stopTime = time;
    let startTime = Date.now();
    let interval;
    let updateIntervalInMillis = 77;
    let forcedStop = false;

    /**
     * @param {number | undefined} seconds
     */
    function setSeconds(seconds) {
        if (isNaN(seconds)) {
            return;
        }

        _seconds = seconds;
        time = time > seconds * 1000 ? time : seconds * 1000;
    }

    /**
     * @param {number | undefined} seconds
     */
    function start() {
        forcedStop = false;
        if (state === TimerState.RUNNING) {
            return;
        }

        beforeStart(time);

        logger.log('started');
        startTime = Date.now();
        state = TimerState.RUNNING;

        run();
    }

    function stop(force = false) {
        forcedStop = force;
        if (state === TimerState.STOPPED) {
            return;
        }

        logger.log(forcedStop ? 'forced stop' : 'stopped');

        state = TimerState.STOPPED;
        stopTime = time;
    }

    /**
     * @param {number | undefined} seconds
     */
    function reset() {
        logger.log('reset');
        forcedStop = false;

        startTime = Date.now();
        time = _seconds * 1000;
        stopTime = time;

        onReset(time);
    }

    function run() {
        if (interval) {
            logger.log('interval already existed, force stop');
            clearInterval(interval);
        }
        interval = setInterval(() => {
            if (state === TimerState.STOPPED) {
                if (!forcedStop) {
                    afterStop(time);
                }
                clearInterval(interval);
                interval = null;
                return;
            }

            const millis = stopTime - Math.max(Date.now() - startTime, 0);

            if (_seconds && millis <= 0) {
                time = 0;
                onChange(time);
                stop();
                return;
            }

            time = Math.abs(millis);

            onChange(time);
        }, updateIntervalInMillis);
    }

    return Object.freeze({
        id,
        start,
        stop,
        reset,
        setSeconds
    });
};
