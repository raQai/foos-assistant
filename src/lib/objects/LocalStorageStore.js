import Logger from '$lib/objects/Logger.js';
import { browser } from '$app/environment';
import { writable, get } from 'svelte/store';

function logger(keyName) {
    return Logger(`LocalStorageStore#${keyName}`);
}

export function localStorageWritable(keyName, data) {
    const store = writable(data);

    const { subscribe, set } = store;

    let storedValue = data;
    if (browser) {
        storedValue = window.localStorage.getItem(keyName) ?? data;
        logger(keyName).log('read', storedValue);
    }
    if (storedValue) {
        try {
            set(JSON.parse(storedValue));
        } catch {
            set(storedValue);
        }
    }

    return {
        subscribe,
        set: (value) => {
            if (browser) {
                window.localStorage.setItem(keyName, JSON.stringify(value));
                logger(keyName).log('set', value);
            }
            return set(value);
        },
        update: (callback) => {
            const updatedStore = callback(get(store));

            if (browser) {
                window.localStorage.setItem(keyName, JSON.stringify(updatedStore));
                logger(keyName).log('update', updatedStore);
            }
            return set(updatedStore);
        },
        get: () => get(store)
    };
}
