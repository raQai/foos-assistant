export default (name) => ({
    log: (...args) => console.log(`[${name}]:`, ...args)
});
